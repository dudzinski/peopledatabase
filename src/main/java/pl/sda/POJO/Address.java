package pl.sda.POJO;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Table(name = "address")
public class Address implements Serializable {

    @Column(name = "street", nullable = false)
    private String street;
    @Column(name = "home_number", nullable = false)
    private String homeNumber;
    @Column(name = "local_number")
    private String localNumber;
    @Column(name = "city", nullable = false)
    private String city;

    public Address() {
    }

    public Address(String street, String homeNumber, String localNumber, String city) {
        this.street = street;
        this.homeNumber = homeNumber;
        this.localNumber = localNumber;
        this.city = city;
    }

    @Override
    public String toString() {
        return "\nAddress{" +
                "street='" + street + '\'' +
                ", homeNumber='" + homeNumber + '\'' +
                ", localNumber='" + localNumber + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(getStreet(), address.getStreet()) &&
                Objects.equals(getHomeNumber(), address.getHomeNumber()) &&
                Objects.equals(getLocalNumber(), address.getLocalNumber()) &&
                Objects.equals(getCity(), address.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStreet(), getHomeNumber(), getLocalNumber(), getCity());
    }

    public String getLocalNumber() {
        return localNumber;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
