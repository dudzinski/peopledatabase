package pl.sda.POJO;

public enum Education {

    PRIMARY("primary"), SECONDARY("secondary"), HIGHER("higher");

    String education;

    Education(String education) {
        this.education = education;
    }

    public String getEducation() {
        return education;
    }
}
