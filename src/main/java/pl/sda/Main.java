package pl.sda;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.sda.Dao.DAO;
import pl.sda.Dao.DataManager;
import pl.sda.POJO.Address;
import pl.sda.POJO.Education;
import pl.sda.POJO.Gender;
import pl.sda.POJO.Person;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        initialData();
        PanelManager panelManager = new PanelManager();
        panelManager.menuPanel(primaryStage);

    }

    public static void main(String[] args) {
        launch(args);
    }

    public void initialData() {
        DAO DAOManager = new DataManager();

        Address address = new Address();
        address.setCity("Warsaw");
        address.setStreet("Opolska");
        address.setHomeNumber("5");
        address.setLocalNumber("5");

        Person person = new Person();
        person.setName("Adam");
        person.setSurname("Nowak");
        person.setAge("22");
        person.setGender(Gender.MALE.getGender());
        person.setEducation(Education.HIGHER.getEducation());
        person.setAddress(address);


        DAOManager.addPerson(person);

        Address address2 = new Address();
        address2.setCity("Wroclaw");
        address2.setStreet("Radomska");
        address2.setHomeNumber("1");
        address2.setLocalNumber("1");

        Person person2 = new Person();
        person2.setName("Ewa");
        person2.setSurname("Kowalska");
        person2.setAge("33");
        person2.setGender(Gender.FEMALE.getGender());
        person2.setEducation(Education.HIGHER.getEducation());
        person2.setAddress(address2);

        DAOManager.addPerson(person2);

    }
}

