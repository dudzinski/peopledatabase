package pl.sda;


import pl.sda.POJO.Person;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PropertiesLoadSave {

    public void findByParameterWriter(String parameter){
        Properties properties = new Properties();
        try {
            FileWriter fileWriter = new FileWriter("findBy.properties");
            properties.setProperty("findBy", parameter);
            properties.store(fileWriter,"");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String findByParameterReader(){
        Properties properties = new Properties();

        try {
            FileReader fileReader = new FileReader("findBy.properties");
            properties.load(fileReader);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return properties.getProperty("findBy");

    }

    public void personWriter(Person person){
        Properties properties = new Properties();
        try {
            FileWriter fileWriter = new FileWriter("personFullData.properties");
            properties.setProperty("id", String.valueOf(person.getId()));
            properties.setProperty("name", person.getName());
            properties.setProperty("surname", person.getSurname());
            properties.setProperty("age", person.getAge());
            properties.setProperty("gender", person.getGender());
            properties.setProperty("education", person.getEducation());
            properties.store(fileWriter,"");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Person personReader(){
        Properties properties = new Properties();
        Person person = new Person();

        try {
            FileReader fileReader = new FileReader("personFullData.properties");
            properties.load(fileReader);
        } catch (Exception e) {
            e.printStackTrace();
        }
        person.setId(Integer.parseInt(properties.getProperty("id")));
        person.setName(properties.getProperty("name"));
        person.setSurname(properties.getProperty("surname"));
        person.setAge(properties.getProperty("age"));
        person.setGender(properties.getProperty("gender"));
        person.setEducation(properties.getProperty("education"));

        return person;

    }
}
