package pl.sda.Dao;

import pl.sda.POJO.Person;

import java.util.List;

public interface DAO {

    List<String> listBasicPeopleInfo();

    List<Person> checkDetailsOfGivenPerson(String columnName, String parameter);

    int editPerson(Person person);

    void removePerson(int id);

    int addPerson(Person person);


}
