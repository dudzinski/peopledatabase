package pl.sda.Dao;

import pl.sda.Dao.DAO;
import pl.sda.POJO.Person;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class DataManager implements DAO {

    private final static EntityManagerFactory entityManagerFactory;

    static {
        entityManagerFactory = Persistence.createEntityManagerFactory("jpa-dao");
    }


    @Override
    public List<String> listBasicPeopleInfo() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String basicInfo = "";
        List<String> basicInfos = new ArrayList<>();
        List<Person> persons = entityManager.createQuery("from Person", Person.class).getResultList();

        for (Person p : persons) {
            basicInfo = p.getId() + " " + p.getName() + " " + p.getSurname();
            basicInfos.add(basicInfo);
        }

        entityManager.close();
        return basicInfos;
    }

    @Override
    public List<Person> checkDetailsOfGivenPerson(String columnName, String quote) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String query = "select p from Person p where p."+columnName+" =?1";
        List<Person> person = entityManager.createQuery(query, Person.class)
                .setParameter(1, quote).getResultList();

        System.out.println("DATA MANAGER FOUND : " + person + "\n");

        entityManager.close();
        return person;
    }

    @Override
    public int editPerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = null;


        entityManager.close();
        return person.getId();
    }

    @Override
    public void removePerson(int id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            Person person = entityManager.find(Person.class, id);
            entityManager.remove(person);

            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }

        entityManager.close();
    }

    @Override
    public int addPerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.persist(person);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }

        entityManager.close();
        return person.getId();
    }


}
