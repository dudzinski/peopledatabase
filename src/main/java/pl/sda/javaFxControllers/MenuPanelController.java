package pl.sda.javaFxControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.sda.PanelManager;
import pl.sda.PropertiesLoadSave;

import javax.xml.crypto.Data;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MenuPanelController implements Initializable {

    @FXML
    public Parent menuPanel;
    @FXML
    public Button listBasicLabel;
    @FXML
    public Label findByLabel;
    @FXML
    public Button findByNameLabel;
    @FXML
    public Button findBySurnameLabel;
    @FXML
    public Button findByAgeLabel;
    @FXML
    public Button findByGenderLabel;
    @FXML
    public Button FindByEducationLabel;
    @FXML
    public Button addPersonLabel;
    @FXML
    public Button quitLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }


    public void listBasicDataClick(MouseEvent mouseEvent) {

        // DAO dataManager = new DataManager();
        // List<String> basicDataArray = new ArrayList<>();
        //  basicDataArray.addAll(dataManager.listBasicPeopleInfo());
        // System.out.println(basicDataArray);
        PanelManager panelManager = new PanelManager();
        panelManager.basicDataPanel(new Stage());
        // System.out.println("----------------------------");

        //  System.exit(0);

    }

    public void findByName(MouseEvent mouseEvent) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        propertiesLoadSave.findByParameterWriter("name");
        PanelManager panelManager = new PanelManager();
        panelManager.findByPanel(new Stage(), "name");


    }

    public void findBySurname(MouseEvent mouseEvent) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        propertiesLoadSave.findByParameterWriter("surname");
        PanelManager panelManager = new PanelManager();
        panelManager.findByPanel(new Stage(), "surname");

    }

    public void findByAge(MouseEvent mouseEvent) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        propertiesLoadSave.findByParameterWriter("age");
        PanelManager panelManager = new PanelManager();
        panelManager.findByPanel(new Stage(), "age");

    }

    public void findByEducation(MouseEvent mouseEvent) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        propertiesLoadSave.findByParameterWriter("education");
        PanelManager panelManager = new PanelManager();
        panelManager.findByPanel(new Stage(), "education");

    }

    public void findByGender(MouseEvent mouseEvent) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        propertiesLoadSave.findByParameterWriter("gender");
        PanelManager panelManager = new PanelManager();
        panelManager.findByPanel(new Stage(), "gender");

    }
}
