package pl.sda.javaFxControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.sda.Dao.DataManager;
import pl.sda.PanelManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BasicDataPanelController implements Initializable {

    @FXML
    public Parent basicDataPanel;
    @FXML
    public TextArea textAreaData;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        DataManager dataManager = new DataManager();
        List<String> basicDataString = dataManager.listBasicPeopleInfo();

      //  textAreaData.setText("BASIC DATA START");
        String infoDisplay = "BASIC DATA START\n\n";
        for (int i=0;i<basicDataString.size(); i++) {
            infoDisplay += basicDataString.get(i).toUpperCase() + "\n";
        }
        textAreaData.setText(infoDisplay + "\nBASIC DATA STOP");


    }

    public void quitBasicData(MouseEvent mouseEvent) {

        PanelManager panelManager = new PanelManager();
        panelManager.menuPanel(new Stage());
    }
}
