package pl.sda.javaFxControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import pl.sda.POJO.Person;
import pl.sda.PropertiesLoadSave;

import java.net.URL;
import java.util.ResourceBundle;

public class FullDataPanelController implements Initializable {

    @FXML
    Parent fullDataPanel;
    @FXML
    public Label id;
    @FXML
    private Label name;
    @FXML
    private Label surname;
    @FXML
    private Label age;
    @FXML
    private Label gender;
    @FXML
    private Label education;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        Person person = propertiesLoadSave.personReader();

        id.setText(String.valueOf(person.getId()));
        name.setText(person.getName());
        surname.setText(person.getSurname());
        age.setText(person.getAge());
        gender.setText(person.getGender());
        education.setText(person.getEducation());


    }
}
