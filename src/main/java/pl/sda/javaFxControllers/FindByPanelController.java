package pl.sda.javaFxControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.sda.Dao.DataManager;
import pl.sda.POJO.Person;
import pl.sda.PanelManager;
import pl.sda.PropertiesLoadSave;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class FindByPanelController implements Initializable {

    @FXML
    public Parent findByPanel;
    @FXML
    public javafx.scene.control.TextArea parameterTextArea;
    @FXML
    public Button findByClick;
    @FXML
    public Button quitFindByPanelButton;
    @FXML
    public Label parameterLabel;



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        String byParameterString = propertiesLoadSave.findByParameterReader();
        parameterLabel.setText(byParameterString.toUpperCase());

    }

    public void quitFindByPanel(MouseEvent mouseEvent) {

        PanelManager panelManager = new PanelManager();
        panelManager.menuPanel(new Stage());
    }


    public void findBy(MouseEvent mouseEvent) {

        PanelManager panelManager = new PanelManager();
        DataManager dataManager = new DataManager();
        String parameter = parameterLabel.getText().toLowerCase();
        String parameterValue = parameterTextArea.getText();
        List<Person> persons= dataManager.checkDetailsOfGivenPerson(parameter, parameterValue);

        PropertiesLoadSave propertiesLoadSave = new PropertiesLoadSave();
        propertiesLoadSave.personWriter(persons.get(0));

        panelManager.fullDataPanel(new Stage());


    }
}

